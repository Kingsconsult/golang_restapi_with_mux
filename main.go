package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"math/rand"
	"net/http"
	"strconv"
)

// Book Struct (model
type Book struct {
	ID     string  `json:"id"`
	Isbn   string  `json:"isbn"`
	Title  string  `json:"title"`
	Auther *Auther `json:"author"`
}

type Auther struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

// Init books var as a slice Book Struct
var books []Book

// Get all books
func getBooks(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "Application/json")
	json.NewEncoder(w).Encode(books)
}

// Get one books
func getBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "Application/json")
	params := mux.Vars(r) // Get Params

	// Loop through books and find the one with the needed id
	for _, items := range books {
		if items.ID == params["id"] {
			json.NewEncoder(w).Encode(items)
			return
		}
	}
	json.NewEncoder(w).Encode(&Book{})
}

// Create a books
func createBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "Application/json")
	var book Book
	_ = json.NewDecoder(r.Body).Decode(&book)
	book.ID = strconv.Itoa(rand.Intn(1000000000))
	books = append(books, book)
	json.NewEncoder(w).Encode(book)
}

// Get all books
func updateBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "Application/json")

	params := mux.Vars(r) // Get Params

	// Loop through books and find the one with the needed id
	for index, items := range books {
		if items.ID == params["id"] {

			books = append(books[:index], books[index+1:]...)
			var book Book
			_ = json.NewDecoder(r.Body).Decode(&book)
			book.ID =  params["id"]
			books = append(books, book)
			json.NewEncoder(w).Encode(book)
			return
		}
	}

}

// Get all books
func deleteBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "Application/json")
	params := mux.Vars(r) // Get Params
	for index, item := range books {
		if item.ID == params["id"] {

			books = append(books[:index], books[index+1:]...)
			break
		}
	}
	json.NewEncoder(w).Encode(books)

}

func main() {
	// Init Router
	r := mux.NewRouter()

	// Mock data
	books = append(books, Book{ID: "1", Isbn: "2345", Title: "PJ", Auther: &Auther{Firstname: "kings", Lastname: "kess"}})
	books = append(books, Book{ID: "2", Isbn: "1234", Title: "Tom and Jerry", Auther: &Auther{Firstname: "Emma", Lastname: "Nzube"}})

	// Routes Handlers / Endpoints
	r.HandleFunc("/api/books", getBooks).Methods("GET")
	r.HandleFunc("/api/books/{id}", getBook).Methods("GET")
	r.HandleFunc("/api/books", createBook).Methods("POST")
	r.HandleFunc("/api/books/{id}", updateBook).Methods("PUT")
	r.HandleFunc("/api/books/{id}", deleteBook).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":8000", r))

}
